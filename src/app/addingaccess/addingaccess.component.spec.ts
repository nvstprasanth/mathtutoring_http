import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddingaccessComponent } from './addingaccess.component';

describe('AddingaccessComponent', () => {
  let component: AddingaccessComponent;
  let fixture: ComponentFixture<AddingaccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddingaccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddingaccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
