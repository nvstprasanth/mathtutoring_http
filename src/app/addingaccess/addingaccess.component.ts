import { Component, OnInit } from "@angular/core";
import {
  NgbPaginationModule,
  NgbAlertModule
} from "@ng-bootstrap/ng-bootstrap";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ScheduleExamComponent } from "../schedule-exam/schedule-exam.component";
import { BookExamPopUpComponent } from "../book-exam-pop-up/book-exam-pop-up.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { toDate } from "@angular/common/src/i18n/format_date";
import { MatGridListModule } from "@angular/material/grid-list";

import { MatIconModule } from "@angular/material/icon";

@Component({
  selector: "app-addingaccess",
  templateUrl: "./addingaccess.component.html",
  styleUrls: ["./addingaccess.component.css"]
})
export class AddingaccessComponent implements OnInit {
  public getdetails: any = [];
  ecomid: any;
  wiuid: string;
  wiuemail: any;
  data: [];
  email: string;
  ecom: string;
  role: string;
  deleteId: any;

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  ngOnInit() {
    if (localStorage.getItem("userType") === "student") {
      this.router.navigate(["bookExam"]);
    } else if (localStorage.getItem("userType") === "test") {
      this.router.navigate(["login"]);
    } else {
      this.getAccessList();
    }
  }

  getAccessList() {
    this.http
      .get("http://wiu.edu/CITR/MathTutoring/getting_ecom_records.php")
      .subscribe(
        (data: any) => {
          this.data = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  accessadding(ecom: any) {
    // console.log(mobile);
    // console.log(this.StudentMobile);
    if (ecom == "") {
      this.openSnackBar("Please Enter the correct ecom id", "OK");
    } else {
      // console.log(
      //   "http://localhost:8888/citr/test.php?&ecom=" +
      //     this.ecomid +
      //     "&wiuid=" +
      //     this.wiuid
      // );
      this.http
        .get(
          "http://wiu.edu/citr/MathTutoring/addingaccess.php?&email=" +
            this.email +
            "&ecom=" +
            this.ecom +
            "&role=" +
            this.role
        )
        .subscribe(
          data => {
            console.log(data, "data");
            this.openSnackBar("Data has been updated", "OK");
            this.getAccessList();
          },
          (error: any) => {
            console.log(error);
          }
        );
    }
  }

  // getaccesslist() {
  //   // console.log("http://wiu.edu/CITR/MathTutoring/getaccesslist.php?read=true");
  //   this.http
  //     .get("http://wiu.edu/CITR/MathTutoring/addingaccess.php?read=true")
  //     .subscribe(
  //       data => {
  //         this.getdetails = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       }
  //     );
  // }

  Delete1(id: any) {
    console.log(id);
    this.deleteId = id;
  }
  Delete() {
    console.log(
      this.http.get(
        "http://wiu.edu/CITR/MathTutoring/deleteaccess.php?id=" + this.deleteId
      )
    );
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/deleteaccess.php?id=" + this.deleteId
      )
      .subscribe(
        data => {
          this.getAccessList();
        },
        error => {
          console.log(error);
        }
      );
  }
  Delete3() {
    throw new Error("Method not implemented.");
  }
}
