import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookExamPopUpComponent } from './book-exam-pop-up.component';

describe('BookExamPopUpComponent', () => {
  let component: BookExamPopUpComponent;
  let fixture: ComponentFixture<BookExamPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookExamPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookExamPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
