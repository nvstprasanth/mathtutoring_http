import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  NgbPaginationModule,
  NgbAlertModule
} from "@ng-bootstrap/ng-bootstrap";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ScheduleExamComponent } from "../schedule-exam/schedule-exam.component";
import { BookExamPopUpComponent } from "../book-exam-pop-up/book-exam-pop-up.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import { toDate } from "@angular/common/src/i18n/format_date";
@Component({
  selector: "app-book-exam",
  templateUrl: "./book-exam.component.html",
  styleUrls: ["./book-exam.component.css"]
})
export class BookExamComponent implements OnInit {
  wiuId: any;
  studentDetails: any = [];
  studentName: any;
  studentEmail: any;
  examDates: any = [];
  deleteId: any;
  StudentMobile: any;
  ES_StartTime: any; //Exam Start Time From Server
  ES_EndTime: any; //Exam Start Time From Server
  examReservationData: any = [];
  TodayDate: any;
  url: any;
  urlArray: any;
  CampusName: any;
  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.url = window.location.href;
    //   console.log("https://wiu.edu/CITR/ExamSchedule/macomb".split("/"));
    // this.urlArray="https://wiu.edu/CITR/ExamSchedule/macomb".split("/");
    this.urlArray = this.url.split("/");
    this.CampusName = this.urlArray[5];
    this.CampusName = "macomb";
    // console.log(this.CampusName);
    this.GetExamTime();
    this.studentDetails = "NA";
    this.wiuId = "919132698";
    let dateIs = new Date();
    let MM = Number(dateIs.getMonth()) + 1;
    this.TodayDate = dateIs.getFullYear() + "/" + MM + "/" + dateIs.getDate();
    // this.TodayDate='2019/7/13';
    // /  console.log(this.TodayDate);
  }

  ngOnInit() {
    if (localStorage.getItem("userType") === "test") {
      this.router.navigate(["login"]);
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
  BookExam(
    c_title: any,
    c_exam: any,
    c_star: any,
    c_short: any,
    examStartDate: any,
    examEndDate: any,
    examTime: any,
    id: any,
    note: any
  ): void {
    this.GetExamTime();
    // console.log(c_exam);
    // console.log(c_exam.split("To", 2));
    this.examDates = c_exam.split("To", 2);
    const dialogRef = this.dialog.open(BookExamPopUpComponent, {
      width: "40%",
      maxHeight: "90vh",
      data: {
        course_title: c_title,
        ExamStartDate: this.examDates[0],
        ExamEndDate: this.examDates[1],
        course_star: c_star,
        courseShort: c_short,
        examStartDateFull: examStartDate,
        examEndDateFull: examEndDate,
        ExamTime: examTime,
        cid: id,
        ES_StartTime: this.ES_StartTime,
        ES_EndTime: this.ES_EndTime,
        sid: this.wiuId,
        note: note
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      //  console.log('The dialog was closed');
      //  console.log(result);
      this.SaveStudentExamDate(result);
    });
  }

  GetExamTime() {
    // console.log(
    //   "http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=ALL&campus=" +
    //     this.CampusName
    // );
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=ALL&campus=" +
        this.CampusName
      )
      .subscribe(
        data => {
          //this.Oldseats=data;
          // console.log(data);
          this.ES_EndTime = data[0].EndTime;
          this.ES_StartTime = data[0].StartTime;

          // console.log("StartTime:"+this.ES_StartTime[0]);
          // this.openSnackBar("SuccessFully Updated","Ok");
        },
        error => {
          console.log(error);
        }
      );
  }
  GetStudentExamSchedule() {
    //   let dateIs = new Date();
    //   let currentTimeHour = dateIs.getHours();
    //   let currentDate = dateIs.getDate();

    //  console.log("Current Time:"+currentDate);
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/.sphp?wiuId=" +
        this.wiuId
      )
      .subscribe(
        data => {
          //   this.studentName=data[0].s_name;
          //   this.studentEmail=data[0].s_mail;
          //  this.studentDetails=data;
          this.examReservationData = data;
          //   console.log(this.examReservationData);
        },
        error => {
          console.log(error);
        }
      );
  }
  SaveStudentExamDate(result: any) {
    this.http
      .get(
        "http:///wiu.edu/CITR/MathTutoring/saveStudentExamReservation.sphp?studentName=" +
        this.studentName +
        "&wiuId=" +
        this.wiuId +
        "&emailId=" +
        this.studentEmail +
        "&courseTitle=" +
        result.c_title +
        "&starNumber=" +
        result.courseStar +
        "&examDate=" +
        result.selectedDate +
        "&courseShort=" +
        result.courseShort +
        "&startTime=" +
        result.StartTime +
        "&endTime=" +
        result.EndTime +
        "&cid=" +
        result.cid +
        "&campus=" +
        result.campus
      )
      .subscribe(
        data => {
          //    console.log("Save");
          this.openSnackBar("Tutoring Date is Scheduled", "OK");

          this.http
            .get(
              "http://wiu.edu/CITR/MathTutoring/SendEmail.php?studentName=" +
              this.studentName +
              "&wiuId=" +
              this.wiuId +
              "&emailId=" +
              this.studentEmail +
              "&courseTitle=" +
              result.c_title +
              "&starNumber=" +
              result.courseStar +
              "&examDate=" +
              result.selectedDate +
              "&courseShort=" +
              result.courseShort +
              "&startTime=" +
              result.StartTime +
              "&endTime=" +
              result.EndTime +
              "&cid=" +
              result.cid +
              "&campus=" +
              result.campus
            )
            .subscribe(
              data => {
                //    console.log("Save");
                this.openSnackBar("Email Confirmation Send", "OK");
                //  this.GetStudentExamSchedule();
              },
              error => {
                console.log(error);
              }
            );

          this.GetStudentExamSchedule();
        },
        error => {
          console.log(error);
        }
      );
  }

  UpdateMobile(mobile: any) {
    // console.log(mobile);
    // console.log(this.StudentMobile);
    if (mobile == "") {
      this.openSnackBar("Please Enter correct Mobile Number", "OK");
    } else {
      // console.log("http://wiu.edu/CITR/ExamSchedule/UpdateStudentMobile.sphp?wiuId="+this.wiuId+"mobile="+this.StudentMobile);
      this.http
        .get(
          "http://wiu.edu/CITR/MathTutoring/UpdateStudentMobile.sphp?wiuId=" +
          this.wiuId +
          "&mobile=" +
          this.StudentMobile
        )
        .subscribe(
          data => {
            //   this.studentName=data[0].s_name;
            //   this.studentEmail=data[0].s_mail;
            //   this.StudentMobile=data[0].mobile;
            //  this.studentDetails=data;
            //  this.GetStudentExamSchedule();
            this.openSnackBar("Mobile Updated", "OK");
          },
          error => {
            console.log(error);
          }
        );
    }
  }
  SearchStudent(wiuId: any) {
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getStudentDetails.sphp?wiuId=" +
        this.wiuId
      )
      .subscribe(
        data => {
          this.studentName = data[0].s_name;
          this.studentEmail = data[0].s_mail;
          this.StudentMobile = data[0].mobile;
          this.studentDetails = data;
          // console.log(this.studentDetails);
          //  this.GetStudentExamSchedule();
        },
        error => {
          console.log(error);
        }
      );
    this.GetStudentExamSchedule();
  }
  // SearchStudentByEmail(emailId:any)
  // {

  //   this.http.get("http://wiu.edu/CITR/ExamSchedule/getStudentDetails.sphp?email="+this.)
  //       .subscribe((data) => {
  //         this.studentName=data[0].s_name;
  //         this.studentEmail=data[0].s_mail;
  //         this.StudentMobile=data[0].mobile;
  //        this.studentDetails=data;
  //      //  this.GetStudentExamSchedule();
  //       }, (error) => {

  //           console.log(error);

  //         });
  //         this.GetStudentExamSchedule();
  // }
  getStudentName() {
    return this.studentDetails;
  }
  DeleteExamSchedule(id: any) {
    //  console.log(id);
    this.deleteId = id;
  }
  DeleteExamSchedule_Popup() {
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/deleteStudentExamSchedule.sphp?id=" +
        this.deleteId
      )
      .subscribe(
        data => {
          this.GetStudentExamSchedule();
        },
        error => {
          console.log(error);
        }
      );
  }
}
