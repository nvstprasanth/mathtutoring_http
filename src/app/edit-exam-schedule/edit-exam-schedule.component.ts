import { Component, OnInit } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import { MatSnackBar } from "@angular/material";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";

@Component({
  selector: "app-edit-exam-schedule",
  templateUrl: "./edit-exam-schedule.component.html",
  styleUrls: ["./edit-exam-schedule.component.css"]
})
export class EditExamScheduleComponent implements OnInit {
  starNumber: any = "";
  ExamDetails: any = [];
  CourseTitle: any = "NA";
  Section: any;
  Instructor: any;
  Department: any;
  C_Short: any;

  StartDate: any;
  EndDate: any;
  ExamType: any;
  Note: any;
  ExamTime: any;
  Id: any;
  ExamNo;
  semester: any = "fall";
  term: any;
  CourseShort: any;
  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    console.log(this.semester);
    let temp = new Date();
    if (temp.getMonth() >= 1 && temp.getMonth() < 6) {
      this.term = temp.getFullYear() + "01";
    } else if (temp.getMonth() >= 6 && temp.getMonth() < 8) {
      this.term = temp.getFullYear() + "06";
    } else {
      this.term = temp.getFullYear() + "08";
    }
    console.log(this.term);
    this.SearchCourse();
  }
  onChangeEvent(ev) {
    // console.log("HERE");
    this.semester = ev.target.value;
    console.log(ev.target.value);
    let temp = new Date();
    if (this.semester == "fall") {
      this.term = temp.getFullYear() + "08";
    } else if (this.semester == "spring") {
      this.term = temp.getFullYear() + "01";
    } else if (this.semester == "summer") {
      this.term = temp.getFullYear() + "06";
    } else {
      this.term = "20";
    }
    console.log(this.term);
    this.SearchCourse();
  }
  cancelLoading() {
    this.spinner.hide();
  }
  setMyStyles(i: any) {
    //console.log(i);
    if (i % 2 == 0) {
      return "#d2d2d2";
    }
  }
  SearchCourse() {
    this.ExamDetails.length = 0;
    this.spinner.show();
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getScheduleExamDetails.sphp?starNumber=" +
          this.starNumber +
          "&term=" +
          this.term
      )
      .subscribe(
        data => {
          this.ExamDetails = data;

          if (this.starNumber == "") {
            // this.CourseTitle=data[0].c_title;
            // this.Section=data[0].section;
            // this.Instructor=data[0].instructor;
            // this.Department=data[0].dept;
            // this.C_Short=data[0].c_short;
            // this.ExamDetails=data;
          } else {
            //   this.ExamDetails=data;
            this.CourseTitle = data[0].c_title;
            this.Section = data[0].section;
            this.Instructor = data[0].instructor;
            this.Department = data[0].dept;
            this.C_Short = data[0].c_short;
            //   console.log(data[0].id);
          }
          this.spinner.hide();
        },
        error => {
          console.log(error);
        }
      );
  }

  getBgColor(i: any) {
    // console.log(i);
    if (i % 2 == 0) {
      return "#eaeaea";
    } else {
      return "#ffffff";
    }
  }
  EditExam(i: any, id: any, examId: any) {
    this.Id = id;
    console.log(i);
    this.StartDate = this.ExamDetails[i].startDate;
    this.EndDate = this.ExamDetails[i].endDate;
    this.ExamType = this.ExamDetails[i].examType;
    this.Note = this.ExamDetails[i].note;
    this.ExamTime = this.ExamDetails[i].examTime;
    this.ExamNo = examId;
    this.CourseShort = this.ExamDetails[i].c_title;
    console.log(i + "--" + id + "---" + examId + "--" + this.StartDate);
  }
  DeleteExamSchedule(id: any) {
    console.log(
      "http://wiu.edu/CITR/MathTutoring/getScheduleExamDetails.sphp?delete=true&examTime=&id=" +
        this.Id
    );
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getScheduleExamDetails.sphp?delete=true&examTime=&id=" +
          this.Id
      )
      .subscribe(
        data => {
          this.SearchCourse();
          this.openSnackBar("Delete Successfully!!!", "OK");
        },
        error => {
          console.log(error);
        }
      );
  }
  UpdateExamSchedule() {
    let StartDateIs = new Date(this.StartDate);
    let EndDateIs = new Date(this.EndDate);
    // let NewLine=@"\n";//http://wiu.edu/CITR/ExamSchedule/getScheduleExamDetails.sphp?update=true&examTime=60&startDate=2019/7/10&endDate=2019/7/15&examType=Online&note=One%20line.\nAnother\nline&startTimeSlot=Wed%20Jul%2010%202019%2000:00:00%20GMT-0500%20(Central%20Daylight%20Time)&endTimeSlot=Mon%20Jul%2015%202019%2000:00:00%20GMT-0500%20(Central%20Daylight%20Time)&id=151
    //this.Note=this.Note.replace("\n","\\n");
    this.Note = this.Note.replace(new RegExp("\n", "g"), "\\n");
    //  console.log(this.Note);
    //  console.log("http://wiu.edu/CITR/ExamSchedule/getScheduleExamDetails.sphp?update=true&examTime="+this.ExamTime+"&startDate="+this.StartDate+"&endDate="+this.EndDate
    //  +"&examType="+this.ExamType+"&note="+this.Note+"&startTimeSlot="+StartDateIs.toString()+"&endTimeSlot="+EndDateIs.toString()+"&id="+this.Id);
    // console.log("http://wiu.edu/CITR/ExamSchedule/getScheduleExamDetails.sphp?update=true&examTime="+this.ExamTime+"&startDate="+this.StartDate+"&endDate="+this.EndDate
    // +"&examType="+this.ExamType+"&note="+this.Note+"&startTimeSlot="+StartDateIs+"&endTimeSlot="+EndDateIs+"&id="+this.Id);
    // console.log(new Date(this.StartDate));

    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getScheduleExamDetails.sphp?update=true&examTime=" +
          this.ExamTime +
          "&startDate=" +
          this.StartDate +
          "&endDate=" +
          this.EndDate +
          "&examType=" +
          this.ExamType +
          "&note=" +
          this.Note +
          "&startTimeSlot=" +
          StartDateIs.toString() +
          "&endTimeSlot=" +
          EndDateIs.toString() +
          "&id=" +
          this.Id +
          "&cid=" +
          this.ExamNo
      )
      .subscribe(
        data => {
          this.SearchCourse();
          this.openSnackBar("Update Successfull!!!", "OK");
        },
        error => {
          console.log(error);
        }
      );

    this.SearchCourse();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }
  ngOnInit() {
    if (localStorage.getItem("userType") === "test") {
      this.router.navigate(["login"]);
    }
  }
}
