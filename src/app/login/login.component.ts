import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
  HttpParams
} from "@angular/common/http";
import { MatDialog } from "@angular/material";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private http: HttpClient) {}
  username: string;
  password: string;
  httpClient: any;
  errorMessage: string;

  ngOnInit() {
    if (localStorage.getItem("userType") !== "test") {
      this.router.navigate(["bookExam"]);
    }
  }

  login(): void {
    console.log(this.username, this.password, "details");
    this.http
      .get(
        `http://wiu.edu/CITR/MathTutoring/ecom_login.php?email=${this.username}&ecom=${this.password}`
      )
      .subscribe(
        (data: any) => {
          console.log(data, "data");
          if (data.role !== undefined) {
            localStorage.setItem("userType", data.role);
            this.router.navigate(["bookExam"]);
          } else if (data.message) {
            this.errorMessage = data.message;
          }
        },
        error => {
          console.log(error);
        }
      );
    //this.Sav
    // if (this.username === "admin" && this.password === "admin") {
    //   this.router.navigate(["bookExam"]);
    // } else {
    //   alert("Invalid credentials");
    // }
  }
}
