import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonWIUStudentBookSlotComponent } from './non-wiustudent-book-slot.component';

describe('NonWIUStudentBookSlotComponent', () => {
  let component: NonWIUStudentBookSlotComponent;
  let fixture: ComponentFixture<NonWIUStudentBookSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonWIUStudentBookSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonWIUStudentBookSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
