import { Component, OnInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-notes",
  templateUrl: "./notes.component.html",
  styleUrls: ["./notes.component.css"]
})
export class notesComponent implements OnInit {
  DateToSearch: any;
  NotesData: any;
  examType: string;
  FilterNoteDetails: any;
  constructor(
    public http: HttpClient,
    public dialogRef: MatDialogRef<notesComponent>,
    private spinner: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public impdata: any
  ) {
    this.DateToSearch = impdata.dateToSearch;
    this.GetNotesData();
  }

  ngOnInit() {}

  GetNotesData() {
    this.spinner.show();
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getAllStudentReservation.sphp?examDate=" +
          this.DateToSearch +
          "&campus=Macomb"
      )
      .subscribe(
        data => {
          this.NotesData = data;
          const array = this.NotesData;
          const filter = array.filter(arr => {
            return arr.examType !== "Online";
          });
          console.log(filter, "filter");
          const result = Object.values(
            filter.reduce((r, e) => {
              let k = `${e.courseName}`;
              if (!r[k]) r[k] = { ...e, count: 1 };
              else r[k].count += 1;
              return r;
            }, {})
          );

          this.FilterNoteDetails = result;
          this.spinner.hide();
        },
        error => {
          console.log(error);
        }
      );
  }
}
