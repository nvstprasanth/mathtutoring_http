import { Component, QueryList, ViewChildren } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import { OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { getLocaleMonthNames } from "@angular/common";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatStepperModule, MatStepper } from "@angular/material/stepper";
@Component({
  selector: "app-root",
  templateUrl: "./schedule-exam.component.html",
  styleUrls: ["./schedule-exam.component.css"]
})
export class ScheduleExamComponent implements OnInit {
  @ViewChildren("examTime") examTime: QueryList<any>;
  @ViewChildren("types") types: QueryList<any>;
  @ViewChildren("stepper") stepper: MatStepper;

  title = "ExamSchedule";
  starNumber: any;
  courseDetails: any = [];
  isLinear = false;
  NumberOfExam: any;
  NumberOfExamAray: any = [];
  startDate: any = [];
  endDate: any = [];
  ExamTime: any;
  Month_1: any;
  note: any;
  examType: any;
  isEditable: any;
  createSlot = 0;
  picker_1: any;
  picker_2: any;
  SaveBtnStatus: any = [];
  constructor(
    public http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.starNumber = "51341";
    this.isEditable = "true";
    this.dateLoop();
  }
  ngOnInit() {
    if (localStorage.getItem("userType") === "test") {
      this.router.navigate(["login"]);
    }
  }
  nextClicked(event) {
    // complete the current step
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: "center"
    });
  }

  dateLoop() {
    let TimeSlot_1 = new Date("October 13, 2014 " + "8:00");
    let TimeSlot_2 = new Date("October 13, 2014 " + "16:30000");

    let FirstTime = new Date(TimeSlot_1);
    let SecomndTime = new Date(TimeSlot_1);
    SecomndTime.setMinutes(TimeSlot_1.getMinutes() + 60);
    while (SecomndTime <= TimeSlot_2) {
      //  console.log(TimeSlot_1.getHours()+":"+TimeSlot_1.getMinutes()+"---"+SecomndTime.getHours()+":"+SecomndTime.getMinutes());
      SecomndTime.setMinutes(SecomndTime.getMinutes() + 15);
      TimeSlot_1.setMinutes(TimeSlot_1.getMinutes() + 15);
    }
  }
  SaveExam(
    ExamTime: any,
    StartDate: any,
    EndDate: any,
    ExamType: any,
    Note: any,
    stepper: MatStepper,
    TimeSlot_Start: any,
    TimeSlot_End: any,
    i: any
  ) {
    this.isEditable = "false";
    this.SaveBtnStatus[i] = "disable";
    //console.log(TimeSlot_End);
    if (ExamTime != "" && StartDate != "" && EndDate != "" && ExamType != "") {
      let StartDateMonth = Number(StartDate.getMonth()) + 1;
      let StartDateNew =
        StartDate.getFullYear() +
        "/" +
        StartDateMonth +
        "/" +
        StartDate.getDate();

      let EndDateMonth = Number(EndDate.getMonth()) + 1;
      let EndDateNew =
        EndDate.getFullYear() + "/" + EndDateMonth + "/" + EndDate.getDate();
      //   console.log(StartDateNew);

      TimeSlot_Start = TimeSlot_Start + ":00";
      TimeSlot_End = TimeSlot_End + "00";
      let TimeSlot_1 = new Date("October 13, 2014 " + TimeSlot_Start);
      let TimeSlot_2 = new Date("October 13, 2014 " + TimeSlot_End);
      let TimeSlot_1_Hour = TimeSlot_1.getHours();
      let TimeSlot_1_Min = TimeSlot_1.getMinutes();
      StartDate.setHours(TimeSlot_1_Hour);
      StartDate.setMinutes(TimeSlot_1_Min);

      let TimeSlot_2_Hour = TimeSlot_2.getHours();
      let TimeSlot_2_Min = TimeSlot_2.getMinutes();
      EndDate.setHours(TimeSlot_2_Hour);
      EndDate.setMinutes(TimeSlot_2_Min);
      //  console.log(StartDate);
      Note = encodeURIComponent(Note.replace(new RegExp("\n", "g"), "\\n"));
      console.log("Note", Note);
      this.http
        .get(
          "http://wiu.edu/CITR/MathTutoring/saveExam.sphp?starNumber=" +
            this.starNumber +
            "&startDate=" +
            StartDateNew +
            "&endDate=" +
            EndDateNew +
            "&examType=" +
            ExamType +
            "&examTime=" +
            ExamTime +
            "&note=" +
            Note +
            "&courseTitle=" +
            this.courseDetails[0].title +
            "&section=" +
            this.courseDetails[0].section +
            "&term=" +
            this.courseDetails[0].term +
            "&instrutor=" +
            this.courseDetails[0].instructor +
            "&dept=" +
            this.courseDetails[0].department +
            "&course_short=" +
            this.courseDetails[0].course_short +
            "&start_TimeSlot=" +
            StartDate +
            "&ExamDate=0&examBetween=" +
            TimeSlot_Start +
            " TO " +
            TimeSlot_End +
            "&end_TimeSlot=" +
            EndDate
        )
        .subscribe(
          data => {
            console.log(data);
            //this.courseDetails=data;
            this.openSnackBar("Tutoring Scheduled Successfully!!!", "OK");
          },
          error => {
            console.log(error);
          }
        );
    } else {
      this.openSnackBar(
        "Please Enter all Field and Tutor Time should be more than 60 Min",
        "OK"
      );
    }
  }
  // SaveExam(ExamTime:any,StartDate:any,EndDate:any,ExamType:any,Note:any,stepper: MatStepper,TimeSlot_Start:any,TimeSlot_End:any)
  // {
  //   this.isEditable="false";
  //   console.log(TimeSlot_End);
  //   if(ExamTime!=""&&StartDate!=""&&EndDate!=""&&ExamType!=""&&ExamTime>=60)
  //   {
  //    let StartDateMonth=Number(StartDate.getMonth())+1;
  //    let StartDateNew=StartDate.getFullYear()+"/"+StartDateMonth+"/"+StartDate.getDate();

  //    let EndDateMonth=Number(EndDate.getMonth())+1;
  //    let EndDateNew=EndDate.getFullYear()+"/"+EndDateMonth+"/"+EndDate.getDate();
  //    console.log(StartDateNew);

  //    TimeSlot_Start=TimeSlot_Start+":00";
  //    TimeSlot_End=TimeSlot_End+"00";
  //   let TimeSlot_1 = new Date("October 13, 2014 "+TimeSlot_Start);
  //   let TimeSlot_2 = new Date("October 13, 2014 "+TimeSlot_End);

  //   let startDateIs = new Date(StartDateNew);
  //   let endDateIs=new Date(EndDateNew);
  //   let NewDate=new Date(startDateIs);
  //   console.log(NewDate+"<"+endDateIs);
  //   let examDateMonth=Number(NewDate.getMonth())+1;
  //   while(NewDate<=endDateIs)
  //     {
  //       console.log(examDateMonth+"/"+NewDate.getDate());
  //       let TimeSlot_1 = new Date("October 13, 2014 "+TimeSlot_Start);
  //   let TimeSlot_2 = new Date("October 13, 2014 "+TimeSlot_End);
  //       // let NewTime=new Date(TimeSlot_1);
  //       // let startTime=TimeSlot_1.getHours()+":"+TimeSlot_1.getMinutes();
  //       // let FirstTime=TimeSlot_1.getHours()+":"+TimeSlot_1.getMinutes();
  //       // let SecondTime=TimeSlot_2.getHours()+":"+TimeSlot_2.getMinutes();
  //       //console.log(FirstTime+"---"+SecondTime);

  //       let FirstTime=new Date(TimeSlot_1);
  //       let SecomndTime=new Date(TimeSlot_1);
  //       SecomndTime.setMinutes(TimeSlot_1.getMinutes()+ExamTime);
  //       while(SecomndTime<=TimeSlot_2)
  //       {
  //         let examDateIs=NewDate.getFullYear()+"/"+examDateMonth+"/"+NewDate.getDate();
  //         console.log(TimeSlot_1.getHours()+":"+TimeSlot_1.getMinutes()+"---"+SecomndTime.getHours()+":"+SecomndTime.getMinutes());

  //            this.http.get("http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber="+this.starNumber
  //         +"&startDate="+StartDateNew +"&endDate="+EndDateNew +"&examType="+ExamType +"&examTime="+ExamTime +"&note="+Note
  //         +"&courseTitle="+this.courseDetails[0].title+"&section="+this.courseDetails[0].section+"&term="+this.courseDetails[0].term+"&instrutor="+this.courseDetails[0].instructor
  //         +"&dept="+this.courseDetails[0].department+"&course_short="+this.courseDetails[0].course_short+"&start_TimeSlot="+TimeSlot_1.getHours()+":"+TimeSlot_1.getMinutes()+
  //         "&ExamDate="+examDateIs+"&examBetween="+TimeSlot_Start+" TO "+TimeSlot_End+"&end_TimeSlot="+SecomndTime.getHours()+":"+SecomndTime.getMinutes()

  //         )
  //         .subscribe((data) => {
  //          // console.log(data[0].title)
  //          //this.courseDetails=data;
  //           this.openSnackBar("Exam Scheduled Successfully!!!","OK");
  //         }, (error) => {

  //             console.log(error);

  //           });
  //           SecomndTime.setMinutes(SecomndTime.getMinutes()+15);
  //           TimeSlot_1.setMinutes(TimeSlot_1.getMinutes()+15);

  //       }

  //       //  while(NewTime<TimeSlot_2)
  //        // {
  //       //   let TimeSlotIs=NewTime.getHours()+":"+NewTime.getMinutes();
  //       //   let endTimeSlot=new Date(NewTime);
  //       //   endTimeSlot.setMinutes(NewTime.getMinutes()+ExamTime);
  //       //   let EndTImeSlotIs=endTimeSlot.getHours()+":"+endTimeSlot.getMinutes();
  //       //   console.log(NewDate.getFullYear()+"/"+NewDate.getMonth()+"/"+NewDate.getDate());
  //       //   examDateMonth=Number(NewDate.getMonth())+1;
  //       //   let examDateIs=NewDate.getFullYear()+"/"+examDateMonth+"/"+NewDate.getDate();
  //       //   console.log(TimeSlot_Start+"-"+TimeSlot_End);

  //       //   // this.http.get("http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber="+this.starNumber
  //       //   // +"&startDate="+StartDateNew +"&endDate="+EndDateNew +"&examType="+ExamType +"&examTime="+ExamTime +"&note="+Note
  //       //   // +"&courseTitle="+this.courseDetails[0].title+"&section="+this.courseDetails[0].section+"&term="+this.courseDetails[0].term+"&instrutor="+this.courseDetails[0].instructor
  //       //   // +"&dept="+this.courseDetails[0].department+"&course_short="+this.courseDetails[0].course_short+"&start_TimeSlot="+TimeSlotIs+
  //       //   // "&ExamDate="+examDateIs+"&examBetween="+TimeSlot_Start+" TO "+TimeSlot_End+"&end_TimeSlot="+EndTImeSlotIs

  //       //   // )
  //       //   // .subscribe((data) => {
  //       //   //  // console.log(data[0].title)
  //       //   //  //this.courseDetails=data;
  //       //   //   this.openSnackBar("Exam Scheduled Successfully!!!","OK");
  //       //   // }, (error) => {

  //       //   //     console.log(error);

  //       //   //   });
  //         // NewTime.setMinutes(TimeSlot_1.getMinutes()+30);

  //        // }
  //        NewDate.setDate(NewDate.getDate()+1);
  //     }
  //   }
  //   else{
  //     this.openSnackBar("Please Enter all Field and Exam Time should be more than 60 Min","OK");
  //   }

  // }
  // SaveExam_OLD(ExamTime:any,StartDate:any,EndDate:any,ExamType:any,Note:any,stepper: MatStepper,TimeSlot_Start:any,TimeSlot_End:any)
  // {

  //   this.isEditable="false";
  //     console.log(TimeSlot_Start);
  //     if(ExamTime!=""&&StartDate!=""&&EndDate!=""&&ExamType!="")
  //     {

  //     this.startDate= StartDate.toString().split(" ", 4);
  //     this.Month_1=this.getMonth();
  //     StartDate=this.startDate[3]+"/"+this.Month_1+"/"+this.startDate[2];
  //     console.log(StartDate);
  //     this.endDate= EndDate.toString().split(" ", 4);
  //     this.Month_1=this.getMonth();
  //     EndDate=this.endDate[3]+"/"+this.Month_1+"/"+this.endDate[2];
  //     console.log(EndDate);
  //     console.log(ExamType);

  //      let startDateIs = new Date(StartDate);
  //     let endDateIs=new Date(EndDate);
  //     let NewDate=new Date(startDateIs);
  //     //let newDat=dateIs.setDate(dateIs.getDate()+1)

  //   let startTimeIs=TimeSlot_Start+":00";
  //   let endTimeIs=TimeSlot_End+"00";
  //   let dateIs_1 = new Date("October 13, 2014 "+startTimeIs);
  //   let dateIs_2 = new Date("October 13, 2014 "+endTimeIs);

  //     while(NewDate<=endDateIs)
  //     {
  //       console.log(NewDate.getMonth()+"/"+NewDate.getDate());
  //       let examDateIs=NewDate.getFullYear()+"/"+NewDate.getMonth()+"/"+NewDate.getDate();

  //       // console.log("http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber="+this.starNumber
  //       // +"&startDate="+StartDate +"&endDate="+EndDate +"&examType="+ExamType +"&examTime="+ExamTime +"&note="+Note
  //       // +"&courseTitle="+this.courseDetails[0].title+"&section="+this.courseDetails[0].section+"&term="+this.courseDetails[0].term+"&instrutor="+this.courseDetails[0].instructor
  //       // +"&dept="+this.courseDetails[0].department+"&course_short="+this.courseDetails[0].course_short+"&TimeSlot="+TimeSlot+"&ExamDate="+examDateIs+"&examBetween="+examBetween);

  //       let NewTime=new Date(dateIs_1);
  //   let startTime=dateIs_1.getHours()+":"+dateIs_1.getMinutes();
  //       while(NewTime<dateIs_2)
  //       {

  //         let TimeSlotIs=NewTime.getHours()+":"+NewTime.getMinutes();
  //         let endTimeSlot=new Date(NewTime);
  //         endTimeSlot.setMinutes(NewTime.getMinutes()+ExamTime);
  //         let EndTImeSlotIs=endTimeSlot.getHours()+":"+endTimeSlot.getMinutes();
  //         this.http.get("http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber="+this.starNumber
  //         +"&startDate="+StartDate +"&endDate="+EndDate +"&examType="+ExamType +"&examTime="+ExamTime +"&note="+Note
  //         +"&courseTitle="+this.courseDetails[0].title+"&section="+this.courseDetails[0].section+"&term="+this.courseDetails[0].term+"&instrutor="+this.courseDetails[0].instructor
  //         +"&dept="+this.courseDetails[0].department+"&course_short="+this.courseDetails[0].course_short+"&start_TimeSlot="+TimeSlotIs+
  //         "&ExamDate="+examDateIs+"&examBetween="+TimeSlot_Start+"TO"+TimeSlot_End+"&end_TimeSlot="+EndTImeSlotIs

  //         )
  //         .subscribe((data) => {
  //           console.log(data[0].title)
  //          this.courseDetails=data;
  //         }, (error) => {

  //             console.log(error);

  //           });
  //         NewTime.setMinutes(dateIs_1.getMinutes()+ExamTime);

  //       }

  //       NewDate.setDate(NewDate.getDate()+1);

  //     }

  //       stepper.next();
  //   }
  //   else{
  //     console.log('Enter All Field');
  //   }

  // }
  SearchCourse() {
    //  console.log(this.starNumber);
    this.NumberOfExam = 0;
    this.createSlot = 0;
    this.NumberOfExamAray.length = 0;
    this.ExamTime = "";
    this.note = "";
    this.examType = "";
    this.picker_1 = "";
    this.picker_2 = "";

    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getCourseDetails.sphp?starNumber=" +
          this.starNumber
      )
      .subscribe(
        data => {
          //     console.log(data[0].title)
          this.courseDetails = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  CreateExamSlot() {
    this.createSlot = 1;
    for (let i = 0; i < this.NumberOfExam; i++) {
      this.NumberOfExamAray[i] = i + 1;
      this.SaveBtnStatus[i] = "Enable";
    }
  }

  getMonth() {
    if (this.startDate[1] == "Jan" || this.endDate[1] == "Jan") {
      this.Month_1 = "01";
    }
    if (this.startDate[1] == "Feb" || this.endDate[1] == "Jan") {
      this.Month_1 = "02";
    }

    if (this.startDate[1] == "May" || this.endDate[1] == "Jan") {
      this.Month_1 = "05";
    }
    if (this.startDate[1] == "Mar" || this.endDate[1] == "Jan") {
      this.Month_1 = "03";
    }
    if (this.startDate[1] == "Apr" || this.endDate[1] == "Jan") {
      this.Month_1 = "04";
    }
    if (this.startDate[1] == "Jun" || this.endDate[1] == "Jan") {
      this.Month_1 = "06";
    }
    if (this.startDate[1] == "Jul" || this.endDate[1] == "Jan") {
      this.Month_1 = "07";
    }
    if (this.startDate[1] == "Aug" || this.endDate[1] == "Jan") {
      this.Month_1 = "08";
    }
    if (this.startDate[1] == "Sep" || this.endDate[1] == "Jan") {
      this.Month_1 = "09";
    }
    if (this.startDate[1] == "Oct" || this.endDate[1] == "Jan") {
      this.Month_1 = "10";
    }
    if (this.startDate[1] == "Nov" || this.endDate[1] == "Jan") {
      this.Month_1 = "11";
    }
    if (this.startDate[1] == "Dec" || this.endDate[1] == "Jan") {
      this.Month_1 = "12";
    }
    return this.Month_1;
  }
}
