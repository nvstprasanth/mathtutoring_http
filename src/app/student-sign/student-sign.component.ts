import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType, HttpParams } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import {LZStringService} from 'ng-lz-string';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ViewReservationComponent } from '../view-reservation/view-reservation.component';
@Component({
  selector: 'app-student-sign',
  templateUrl: './student-sign.component.html',
  styleUrls: ['./student-sign.component.css']
})
export class StudentSignComponent implements OnInit {
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  StudentSignData:any;
  signData:any;
  testImage:any;
  SaveSignBtn='Enable';
  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    // 'minWidth': 2,
    'canvasWidth': 700,
    'canvasHeight': 400,
   
    
  };
  httpClient: any;
  constructor(public dialogRef: MatDialogRef<StudentSignComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private http:HttpClient,private _sanitizer: DomSanitizer,private lz: LZStringService,private _snackBar: MatSnackBar) { 
   // console.log(data);
  // this.SaveSignBtn='Enable';
    this.StudentSignData=data;
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    // this.signaturePad is now available
    this.signaturePad.resizeCanvas();
    this.signaturePad.set('minWidth', 0.5); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }
 
  ClearTheSign()
  {
    this.signaturePad.clear();
  }
  ClosePopup()
  {
    this.dialogRef.close();
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  SaveSign()
  {
   this.SaveSignBtn='disable';
    // const fd=new FormData();
    // fd.append('image',blob,this.StudentSignData.sid);
    // //  console.log("http://wiu.edu/CITR/ExamSchedule/SaveStudentSignData.sphp?sid="+this.StudentSignData.sid+"&signData="+this.signData);
    //   this.http.post("http://wiu.edu/CITR/ExamSchedule/SaveStudentSignData.sphp",fd)
    // .subscribe((data) => {
     
    // }, (error) => {

    //     console.log(error);

    //   });
    setTimeout(()=>{
    fetch(this.signData)
    .then(res => res.blob())
    .then(blob => {
    //  console.log(blob);
      const fd=new FormData();
        fd.append('image',blob,this.StudentSignData.sid);
        this.http.post("http://wiu.edu/CITR/MathTutoring/SaveStudentSignData.sphp",fd)
        .subscribe((data) => {
         this.openSnackBar("Thank You for Sign","OK");
         
         this.dialogRef.close();
        }, (error) => {
    
            console.log(error);
            this.openSnackBar("Thank You for Sign","OK");
            
            this.dialogRef.close();
          });
      //this.SaveSign(blob);
    });
  },3000)
  }

 


  drawComplete() {
    // will be notified of szimek/signature_pad's onEnd event
  //  console.log(this.signaturePad.toDataURL());
    // this.signData=this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
    // + this.signaturePad.toDataURL());
    this.signData=this.signaturePad.toDataURL();
    this.signaturePad.fromDataURL(this.signData);
    const data = this.signaturePad.toData();
    
    
  




 
  }
  dataURItoBlob(dataURI) {

    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    let ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}
  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }
}
