import {
  Component,
  OnInit,
  ViewChild,
  ModuleWithComponentFactories,
  ɵConsole
} from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import { MatDialog, MatSnackBar, DateAdapter } from "@angular/material";
import { StudentSignComponent } from "../student-sign/student-sign.component";
import { SignaturePad } from "angular2-signaturepad/signature-pad";
import { ToastrService, Toast } from "ngx-toastr";
import { CountdownComponent } from "ngx-countdown";
import { NgxSpinnerService } from "ngx-spinner";
import { MatSlideToggleChange } from "@angular/material";
import { MatRadioChange } from "@angular/material";
import { ActivatedRoute, Router, NavigationStart } from "@angular/router";
import Swal from "sweetalert2";
//import { TimeGraphComponent } from '../time-graph/time-graph.component';
import { adminComponent } from "../admin/admin.component";
import { notesComponent } from "../notes/notes.component";
import { TimeGraphComponent } from "../time-graph/time-graph.component";

@Component({
  selector: "app-view-reservation",
  templateUrl: "./view-reservation.component.html",
  styleUrls: ["./view-reservation.component.css"]
})
export class ViewReservationComponent implements OnInit {
  DateIs_: any;
  flag: any = 0;
  examReservationData: any = [];
  StartTime_Temp: any = 0;
  EndTime_Temp: any = 0;
  Picker: any;
  TempCount: any = 0;
  picker: any;
  checkIn: any;
  deleteId: any;
  CheckInHr: any;
  CheckInMin: any;
  CheckOutHr: any;
  CheckOutMin: any;
  Sid: any;
  warnBg: any;
  ShowTimer: any = [];
  BackgroundColorsForTimer: any = [];
  StartTimer;
  toastRef;
  TimerEvent: any = "pause";
  leftMin: any = ["60", "40", "30"];
  CloseStartTime: any;
  CloseEndTime: any;
  ShowAllOrNotCondition: any = "All";
  DummyexamReservationData: any = [];
  tempFlag = 1;
  DisplayFirstTimer = 1;
  FirstStartTimer = 0;
  FirstEndTimer = 0;
  NoteOnStudent: any = [];
  NoSlotData: any = [];
  checkOut: any;
  SID_ForSlot_Variable: any;
  selectedGroup: any;

  @ViewChild("cd1") counter: CountdownComponent;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  public signaturePadOptions: Object = {
    // passed through to szimek/signature_pad constructor
    // 'minWidth': 2,
    canvasWidth: 400,
    canvasHeight: 400,
    minDistance: 2,
    velocityFilterWeight: 0
  };
  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private snackBar: MatSnackBar
  ) {
    this.flag = 0;
    this.Picker = new Date();
    this.warnBg = "#F44336";
    this.GetStudentsNote();
    // this.showNotification('Start');
    // this.showNotification('Stop');
    //  console.log(this.Picker);
  }
  showNotification(type: any, studentName: any, Msg: any) {
    // this.toastr.success('Hello world!', 'Toastr fun!');
    if (type == "Start")
      this.toastRef = this.toastr.success(Msg, studentName, {
        //  disableTimeOut: true,
        tapToDismiss: false,
        closeButton: true,
        timeOut: 2000
      });
    if (type == "Warning")
      this.toastRef = this.toastr.warning(Msg, studentName, {
        //  disableTimeOut: true,
        tapToDismiss: false,
        closeButton: true,
        timeOut: 2000
      });
    if (type == "Stop")
      this.toastRef = this.toastr.error(Msg, studentName, {
        disableTimeOut: true,
        tapToDismiss: false,
        closeButton: true
      });
    if (type == "Cancel")
      this.toastRef = this.toastr.info(Msg, studentName, {
        // disableTimeOut: true,
        tapToDismiss: false,
        closeButton: true,
        timeOut: 2000
      });
    let audio = new Audio();
    audio.src = "http://wiu.edu/CITR/MathTutoring/sound/plucky.mp3";
    audio.load();
    audio.play();
  }
  NextDate(pickerDate: any) {
    let DateIs = new Date(pickerDate);
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let DateDate = Number(DateIs.getDate()) + 1;
    let Date_ = DateIs.getFullYear() + "/" + DateMonth + "/" + DateDate;
    this.DateIs_ = Date_;
    this.Picker = new Date(this.DateIs_);
    this.getExamReservation(this.DateIs_);
  }
  PrevDate(pickerDate: any) {
    let DateIs = new Date(pickerDate);
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let DateDate = Number(DateIs.getDate()) - 1;
    let Date_ = DateIs.getFullYear() + "/" + DateMonth + "/" + DateDate;
    this.DateIs_ = Date_;
    this.Picker = new Date(this.DateIs_);
    this.getExamReservation(this.DateIs_);
  }
  ngOnInit() {
    this.selectedGroup = "Both";
    if (localStorage.getItem("userType") === "test") {
      this.router.navigate(["login"]);
    }
  }
  getExamReservation(dateIs: any) {
    //this.DateIs_=dateIs;
    console.log(dateIs, "dateeee");
    this.CloseStartTime = "";
    this.CloseEndTime = "";
    this.spinner.show();
    let DateIs = new Date(dateIs);
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let Date_ = DateIs.getFullYear() + "/" + DateMonth + "/" + DateIs.getDate();
    this.DateIs_ = Date_;
    console.log(
      "http://wiu.edu/CITR/MathTutoring/getExamReservation.sphp?examDate=" +
        Date_
    );
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getAllStudentReservation.sphp?examDate=" +
          Date_ +
          "&campus=Macomb"
      )
      .subscribe(
        data => {
          this.examReservationData = data;
          if (this.examReservationData.length == 0) {
            Swal.fire({
              title: "Opss",

              text: "No Tutoring Schedule!",

              //showCancelButton: true,
              confirmButtonColor: "#3085d6",
              // cancelButtonColor: '#d33',
              confirmButtonText: "OK!"
            }).then(result => {
              if (result.value) {
                this.spinner.hide();
              }
            });
          }
          console.log(data);

          let tempExamTime = 0;
          tempExamTime = this.examReservationData[0].StartTime;
          for (let i = 0; i < this.examReservationData.length; i++) {
            this.setTime(i, this.examReservationData[i].checkIn);
            this.BackgroundColorsForTimer[i] = "#3c864647";
            this.ShowTimer[i] = 1;
            this.DummyexamReservationData[i] = this.examReservationData[i];

            if (tempExamTime != this.examReservationData[i].StartTime) {
              this.DisplayFirstTimer = 0;
              console.log("Display:" + this.DisplayFirstTimer);
            }
          }
          if (this.DisplayFirstTimer == 1) {
            this.FirstStartTimer = this.examReservationData[0].StartTime;
            this.FirstEndTimer = this.examReservationData[0].EndTime;
          }
          this.spinner.hide();
          this.flag = 1;

          // console.log("DisplayFirstTime="+this.DisplayFirstTimer);
        },
        error => {
          console.log(error);
          Swal.fire("Oops!", "No Exam Schedule!", "warning");
          this.spinner.hide();
        }
      );

    this.GetNoSlotTime(this.DateIs_);
    //   //Non Wiu Student Reservation
    //   this.http.get("http://wiu.edu/CITR/ExamSchedule/getNonWiuStudentReservation.sphp?examDate="+Date_)
    // .subscribe((data) => {
    //   let NonWiuData:any=[];
    //    NonWiuData=data;
    //   console.log(NonWiuData);

    //   // for(let i=0;i<this.examReservationData.length;i++)
    //   // {
    //   //   this.setTime(i,this.examReservationData[i].checkIn);
    //   //   this.BackgroundColorsForTimer[i]="#3c8646a1"
    //   //   this.ShowTimer[i]=1;

    //   // }
    //   for(let i=this.examReservationData;i<NonWiuData.length;i++)
    //   {
    //     console.log(NonWiuData[i]);
    //     this.examReservationData[i]=NonWiuData[i];
    //   }
    //   this.spinner.hide();
    //   this.flag=1;

    // }, (error) => {

    //     console.log(error);

    //   });
  }

  ShowPassword(i: any) {
    console.log(this.examReservationData[i].password);
    this.openSnackBar(
      "Password:" + this.examReservationData[i].password,
      "Close"
    );
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }

  GetNoSlotTime(dateIs: any) {
    console.log(
      "http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=" +
        dateIs
    );
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=" +
          dateIs +
          "&campus=Macomb"
      )
      .subscribe(
        data => {
          this.NoSlotData = data;
          console.log("Close time" + this.NoSlotData);
          this.CloseStartTime = data[0].StartTime;
          this.CloseEndTime = data[0].EndTime;
        },
        error => {
          console.log(error);
        }
      );
  }

  GetNotSlotReturn() {
    // console.log("TOOOOOOO:"+this.CloseStartTime);
    if (this.CloseStartTime == undefined || this.CloseEndTime == "") {
      return 1;
    } else {
      return 0;
    }
  }

  ShowData() {
    return this.flag;
  }
  SetSlot(StartTime: any, EndTime: any) {
    //console.log(this.examReservationData.length);
    if (this.tempFlag == 1) {
      this.tempFlag = 0;
      return 1;
    }

    if (this.examReservationData.length == 1) {
      return 1;
    }
    // return 1;
    if (this.StartTime_Temp == StartTime && this.EndTime_Temp == EndTime) {
      this.TempCount++;
      // console.log("hereooo");
      return 0;
    } else {
      this.TempCount++;
      this.StartTime_Temp = StartTime;
      this.EndTime_Temp = EndTime;
      // console.log("here");
      return 1;
    }
  }
  StudentEntry(isCheck: any, sid: any, status: any, i: any, checkIn: any) {
    let TodayIs = new Date();
    this.setTime(i, checkIn);

    let CheckIn =
      String("0" + TodayIs.getHours()).slice(-2) +
      ":" +
      String("0" + TodayIs.getMinutes()).slice(-2);
    // console.log(isCheck+"---"+status);
    if (isCheck == "uncheck") {
      if (status == "checkIn") {
        //    console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+sid+"&unCheck=true&checkIn=NULL");
        this.http
          .get(
            "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
              sid +
              "&unCheck=true&checkIn=NULL"
          )
          .subscribe(
            data => {
              // this.examReservationData=data;
              // console.log(data);
              // this.flag=1;
              localStorage.removeItem(this.examReservationData[i].id);
              this.getExamReservation(this.DateIs_);
              this.ShowTimer[i] = 0;
              this.showNotification(
                "Cancel",
                this.examReservationData[i].s_name,
                "Time Cancel"
              );
            },
            error => {
              console.log(error);
            }
          );
      } else {
        this.http
          .get(
            "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
              sid +
              "&unCheck=true&checkOut=NULL"
          )
          .subscribe(
            data => {
              // this.examReservationData=data;
              // console.log(data);
              // this.flag=1;
              this.getExamReservation(this.DateIs_);
            },
            error => {
              console.log(error);
            }
          );
      }
    } else {
      if (status == "checkIn") {
        //  console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+sid+"&unCheck=false&checkIn="+CheckIn);
        this.http
          .get(
            "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
              sid +
              "&unCheck=false&checkIn=" +
              CheckIn
          )
          .subscribe(
            data => {
              // this.examReservationData=data;
              // console.log(data);
              // this.flag=1;
              this.ShowTimer[i] = 1;
              this.showNotification(
                "Cancel",
                this.examReservationData[i].s_name,
                "Exam Start"
              );
              this.getExamReservation(this.DateIs_);
            },
            error => {
              console.log(error);
            }
          );
      } else {
        this.checkOut = CheckIn;
        this.SID_ForSlot_Variable = sid;
        this.http
          .get(
            "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
              sid +
              "&unCheck=false&checkOut=" +
              CheckIn
          )
          .subscribe(
            data => {
              // this.examReservationData=data;
              // console.log(data);
              // this.flag=1;
              this.getExamReservation(this.DateIs_);
              this.OpenSignaturePopup(sid);
            },
            error => {
              console.log(error);
            }
          );
      }
    }
  }

  OpenSignaturePopup(sid: any): void {
    const dialogRef = this.dialog.open(StudentSignComponent, {
      width: "450px",
      data: { sid: sid }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
      console.log("CheckItHEre");
      this.CheckSlotAndOpenIfAvailable();
    });
  }

  CheckSlotAndOpenIfAvailable() {
    console.log(this.checkOut);
    console.log(this.SID_ForSlot_Variable);

    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/CheckMakeSlot.php?sid=" +
          this.SID_ForSlot_Variable +
          "&checkout=" +
          this.checkOut
      )
      .subscribe(
        data => {
          console.log("OK");
        },
        error => {
          console.log(error);
        }
      );
  }

  EditCheckIn(sid: any) {
    this.Sid = sid;
  }
  EditCheckInPopUpConfirm() {
    //  console.log(this.CheckInHr+":"+this.CheckInMin);
    this.CheckInMin = String("0" + this.CheckInMin).slice(-2);
    // console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+this.Sid+"&unCheck=false&checkIn="+this.CheckInHr+":"+this.CheckInMin)
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
          this.Sid +
          "&unCheck=false&checkIn=" +
          this.CheckInHr +
          ":" +
          this.CheckInMin
      )
      .subscribe(
        data => {
          // this.examReservationData=data;
          // console.log(data);
          // this.flag=1;
          this.getExamReservation(this.DateIs_);
        },
        error => {
          console.log(error);
        }
      );
  }

  EditCheckOut(sid: any) {
    this.Sid = sid;
  }
  EditCheckOutPopUpConfirm() {
    // console.log(this.CheckInHr+":"+this.CheckInMin);
    this.CheckOutMin = String("0" + this.CheckOutMin).slice(-2);
    //  console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+this.Sid+"&unCheck=false&checkOut="+this.CheckOutHr+":"+this.CheckOutMin)
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
          this.Sid +
          "&unCheck=false&checkOut=" +
          this.CheckOutHr +
          ":" +
          this.CheckOutMin
      )
      .subscribe(
        data => {
          // this.examReservationData=data;
          // console.log(data);
          // this.flag=1;
          this.getExamReservation(this.DateIs_);
        },
        error => {
          console.log(error);
        }
      );
  }
  UpdateCheckStatus(sid: any, isCheck: any, status: any) {
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/StudentCheckInCheckOut.sphp?sid=" +
          sid +
          "&unCheck=true&checkIn=NULL"
      )
      .subscribe(
        data => {
          // this.examReservationData=data;
          // console.log(data);
          // this.flag=1;
        },
        error => {
          console.log(error);
        }
      );
  }

  DeleteExamSchedule(id: any) {
    // console.log(id);
    this.deleteId = id;
  }
  DeleteExamSchedule_Popup() {
    this.http
      .get(
        "http://wiu.edu/CITR/MathTutoring/deleteStudentExamSchedule.sphp?id=" +
          this.deleteId
      )
      .subscribe(
        data => {
          this.getExamReservation(this.DateIs_);
        },
        error => {
          console.log(error);
        }
      );
  }

  setTime(i: any, checkIn: any) {
    // console.log(this.examReservationData[i].examTime);
    let id = this.examReservationData[i].id;
    console.log("check In:" + checkIn);
    if (localStorage.getItem(id + "-pause")) {
      this.CalcularePauseTime(id);
    }
    if (localStorage.getItem(id)) {
      let examTime =
        Number(this.examReservationData[i].examTime) +
        Number(localStorage.getItem(id));
      if (checkIn != null) {
        let checkInData = checkIn.split(":");

        let temp = new Date();
        temp.setHours(checkInData[0]);
        temp.setMinutes(checkInData[1]);
        console.log("MMM---" + temp);

        let tempDate =
          "Thu Sept 13 2019 " +
          checkIn +
          ":00 GMT-0500 (Central Daylight Time)";
        //  // this.getExamReservation(this.DateIs_)

        let TimerTime = new Date(temp);
        //  console.log(TimerTime);
        let CurrentTime = new Date();
        //  console.log(CurrentTime);
        let currentTimeHour = CurrentTime.getHours();
        let currentTimeMin = CurrentTime.getMinutes();
        let currentOnlyTime = currentTimeHour + ":" + currentTimeMin;
        //  console.log(currentTimeHour+":"+currentTimeMin);
        let TimeDiff = Math.abs(TimerTime.getTime() - CurrentTime.getTime());
        let TimeSpend = Math.floor(TimeDiff / 60000);
        console.log("Remaining Time:" + TimeSpend);
        let TimeLeft = examTime - TimeSpend;
        // console.log(TimeLeft);

        this.leftMin[i] = TimeLeft;
      }
    } else {
      let examTime = Number(this.examReservationData[i].examTime);
      if (checkIn != null) {
        let checkInData = checkIn.split(":");

        let temp = new Date();
        temp.setHours(checkInData[0]);
        temp.setMinutes(checkInData[1]);
        console.log("MMM---" + temp);

        let tempDate =
          "Thu Sept 13 2019 " +
          checkIn +
          ":00 GMT-0500 (Central Daylight Time)";
        //  // this.getExamReservation(this.DateIs_)

        let TimerTime = new Date(temp);
        //  console.log(TimerTime);
        let CurrentTime = new Date();
        //  console.log(CurrentTime);
        let currentTimeHour = CurrentTime.getHours();
        let currentTimeMin = CurrentTime.getMinutes();
        let currentOnlyTime = currentTimeHour + ":" + currentTimeMin;
        //  console.log(currentTimeHour+":"+currentTimeMin);
        let TimeDiff = Math.abs(TimerTime.getTime() - CurrentTime.getTime());
        let TimeSpend = Math.floor(TimeDiff / 60000);
        console.log("Remaining Time:" + TimeSpend);
        let TimeLeft = examTime - TimeSpend;
        // console.log(TimeLeft);

        this.leftMin[i] = TimeLeft;
      }
      //  let TotalTimeSpend=0;
      //   if(checkIn!=null)
      //   {
      //   let checkInData=checkIn.split(":");
      //   let currentOnlyTimeData=currentOnlyTime.split(":");
      //   // let remainingMin=Math.abs(checkInData[1]-currentTimeMin);
      //   // console.log(Math.abs(remainingMin));
      //   // let remainingHr=Math.abs(checkInData[0]-currentTimeHour);
      //   // console.log(Math.abs(remainingHr));
      //   // let remainingHrToMin=0;
      //   // if(remainingHr>0)
      //   // {
      //   //    remainingHrToMin=remainingHr*60;
      //   // }
      //   //  TotalTimeSpend=remainingHrToMin+remainingMin;
      //   // console.log(TotalTimeSpend);

      //   // }

      //   let TimeLeft=examTime-TotalTimeSpend;
      //   this.leftMin[i]=TimeLeft;
      // }
    }
    //  console.log(this.leftMin[i])
  }
  CalcularePauseTime(id) {
    // let lastPauseTime=Number(localStorage.getItem(id));
    //     let myItem = localStorage.getItem(id+"-pause");
    //   let pauseTime = new Date(myItem);
    //    let currentTime=new Date();
    //    console.log(currentTime);
    //    let TimeDiff=Math.abs(currentTime.valueOf()-pauseTime.valueOf());
    //    let PauseTimeDiff=Math.floor(TimeDiff/(1000*60))%60;
    //    let TotalPauseTime=lastPauseTime+PauseTimeDiff;
    //    localStorage.setItem(id,TotalPauseTime.toString());
    //   console.log(PauseTimeDiff);
  }

  getBgColor(i: any, star: any) {
    //  console.log(i);

    if (star == "") {
      return "#c6e1ec";
    } else if (i % 2 == 0) {
      return "#eaeaea";
    }
  }
  onNotify($event: any, i: any) {
    //console.log($event);
    this.warnBg = "#3f51b5a3";
    this.BackgroundColorsForTimer[i] = "#f5a118b0";
    this.showNotification(
      "Warning",
      this.examReservationData[i].s_name,
      $event / 1000 + " Sec Left"
    );
    if ($event == 1000) {
      this.BackgroundColorsForTimer[i] = "#f44336a1";
      this.showNotification(
        "Stop",
        this.examReservationData[i].s_name,
        "Time Over"
      );
    }
  }
  StopTimer(i: any) {
    this.ShowTimer[i] = 0;
    this.showNotification(
      "Start",
      this.examReservationData[i].s_name,
      "Exam Completed"
    );
    localStorage.removeItem(this.examReservationData[i].id);
  }
  onFinished(i: any) {
    //console.log("Finish");
    localStorage.removeItem(this.examReservationData[i].id);
    this.showNotification(
      "Stop",
      this.examReservationData[i].s_name,
      "Time Over"
    );
    this.BackgroundColorsForTimer[i] = "#f44336a1";
  }
  // getTimerColor(tileLeft:any)
  // {
  //     console.log(tileLeft);
  // }

  PauseTimer(i: any, $event: { action: string }, id: any) {
    // console.log($event.action);
    if ($event.action == "pause") {
      let PauseTime = new Date();
      localStorage.setItem(id + "-pause", PauseTime.toString());
      let myItem = localStorage.getItem(id + "-pause");
      // console.log(myItem);
    } else if ($event.action == "resume") {
      let lastPauseTime = Number(localStorage.getItem(id));
      let myItem = localStorage.getItem(id + "-pause");
      let pauseTime = new Date(myItem);
      let currentTime = new Date();
      console.log(currentTime);
      let TimeDiff = Math.abs(currentTime.valueOf() - pauseTime.valueOf());
      let PauseTimeDiff = Math.floor(TimeDiff / (1000 * 60)) % 60;
      let TotalPauseTime = lastPauseTime + PauseTimeDiff;
      localStorage.setItem(id, TotalPauseTime.toString());
      console.log(PauseTimeDiff);
      localStorage.removeItem(id + "-pause");
    }
  }
  radioChange(event: MatRadioChange) {
    console.log(event.source.name, event.value);
    this.ShowAllOrNotCondition = event.value;
    let count = 0;
    if (event.value == "NotCompleted") {
      this.examReservationData.length = 0;
      console.log(this.DummyexamReservationData);
      for (let i = 0; i < this.DummyexamReservationData.length; i++) {
        if (this.DummyexamReservationData[i].checkOut == null) {
          this.examReservationData[count] = this.DummyexamReservationData[i];

          console.log(count);
          count++;
        }
      }
    } else {
      for (let i = 0; i < this.DummyexamReservationData.length; i++) {
        this.examReservationData[i] = this.DummyexamReservationData[i];
      }
    }
    console.log(this.examReservationData);

    if (event.source.name === "radioOpt1") {
      // Do whatever you want here
    }
  }

  ResumeTimer(i: any) {}
  getBtnEvent(event: any) {
    if (event == "play") {
      return "play";
    } else {
      return "pause";
    }
    //return event;
  }

  GetStudentsNote() {
    this.http
      .get("http://wiu.edu/CITR/MathTutoring/saveNoteOnStudent.php")
      .subscribe(
        data => {
          this.NoteOnStudent = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  CheckStudentNote(ecom) {
    for (let i = 0; i < this.NoteOnStudent.length; i++) {
      if (this.NoteOnStudent[i].ecom == ecom) {
        console.log("Found Thief:" + ecom);
        return 0;
      }
    }
    return 1;
  }
  ShowStudentNote(ecom) {
    let TempNote = "";
    for (let i = 0; i < this.NoteOnStudent.length; i++) {
      if (this.NoteOnStudent[i].ecom == ecom) {
        TempNote = TempNote + "<br>" + this.NoteOnStudent[i].note;
      }
    }
    Swal.fire("Student Note", TempNote, "warning");
  }
  async AddNote(ecom) {
    const { value: text } = await Swal.fire({
      title: "Write Note",
      input: "textarea",
      confirmButtonText: "Save",

      inputPlaceholder: "Type your message here...",
      inputAttributes: {
        "aria-label": "Type your message here"
      },
      showCancelButton: true
    });

    if (text) {
      // Swal.fire(text)
      this.http
        .get(
          "http://wiu.edu/CITR/MathTutoring/saveNoteOnStudent.php?wiuId=" +
            ecom +
            "&note=" +
            text
        )
        .subscribe(
          data => {
            console.log("Save");
            this.getExamReservation(this.DateIs_);
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  ShowGraph(picker: any) {
    console.log(picker, "picker");
    let DateIs = new Date(picker);
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let Date_ = DateIs.getFullYear() + "/" + DateMonth + "/" + DateIs.getDate();

    const dialogRef = this.dialog.open(adminComponent, {
      width: "90%",
      maxHeight: "90vh",
      data: { dateToSearch: Date_ }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log("The dialog was closed");
    //   //  console.log(result);
    // });
  }

  ShowNotes(option: any) {
    let DateIs = new Date(option);
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let Date_ = DateIs.getFullYear() + "/" + DateMonth + "/" + DateIs.getDate();
    const dialogRef = this.dialog.open(notesComponent, {
      width: "90%",
      maxHeight: "90vh",
      data: { dateToSearch: Date_, examType: this.selectedGroup }
    });
  }

  getVal() {
    console.log(this.selectedGroup); // returns selected object
  }
}
